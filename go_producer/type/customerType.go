package _type

type CustomerClientResponseType struct {
	Name    string  `json:"name"`
	Email   string  `json:"email"`
	Address Address `json:"address"`
}

type CustomersClientResponseType struct {
	TotalItemCount int                          `json:"totalItemCount"`
	PageNumber     int                          `json:"pageNumber"`
	PageSize       int                          `json:"pageSize"`
	Data           []CustomerClientResponseType `json:"data"`
}

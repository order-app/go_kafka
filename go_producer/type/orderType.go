package _type

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type OrdersClientResponseType struct {
	TotalItemCount int                       `json:"totalItemCount"`
	PageNumber     int                       `json:"pageNumber"`
	PageSize       int                       `json:"pageSize"`
	Data           []OrderClientResponseType `json:"data"`
}

type OrderClientResponseType struct {
	CustomerId primitive.ObjectID `json:"customerId"`
	Quantity   int                `json:"quantity"`
	Price      float64            `json:"price"`
	Status     int                `json:"status"`
	Address    Address            `json:"address"`
	Product    ProductReqRes      `json:"product"`
}

type ProductReqRes struct {
	ImageUrl string `json:"imageUrl"`
	Name     string `json:"name"`
}

package main

import (
	"github.com/labstack/echo"
	"go_kafka/go_producer/client"
	"go_kafka/go_producer/config"
	"go_kafka/go_producer/handler"
	customMiddleware "go_kafka/go_producer/helper/middleware"
	"go_kafka/go_producer/producer"
)

const (
	brokerAddress = "localhost:9092"
)

var clientCnfg config.ClientURIConfig

func init() {
	clientCnfg = config.EnvClientURIConfig["local"]
}

func main() {
	topic := "purchases"
	partition := 0
	kafkaCustomer := producer.NewKafkaClient(brokerAddress, topic, partition)
	defer kafkaCustomer.CloseConnection()

	customerClient := client.NewCustomerClient(clientCnfg.CustomerClient)
	customerHandler := handler.NewCustomerHandler(kafkaCustomer, customerClient)

	orderClient := client.NewOrderClient(clientCnfg.OrderClient)
	orderHandler := handler.NewOrderHandler(kafkaCustomer, orderClient)

	e := echo.New()
	e.Use(customMiddleware.Recovery)
	e.GET("/producer/customer", customerHandler.CustomersHandler)
	e.GET("/producer/order", orderHandler.OrderHandler)
	e.Start("localhost:9090")
}

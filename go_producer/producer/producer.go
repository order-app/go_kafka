package producer

import (
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"log"
	"time"
)

type KafkaClient struct {
	conn *kafka.Conn
}

func NewKafkaClient(brokerAddress string, topic string, partition int) KafkaClient {
	c := createConnection(topic, partition, brokerAddress)
	return KafkaClient{conn: c}
}

func createConnection(topic string, partition int, brokerAddress string) *kafka.Conn {
	conn, err := kafka.DialLeader(context.Background(), "tcp", brokerAddress, topic, partition)
	if err != nil {
		log.Panicf("Failed to dial leader: %v", err.Error())
	}
	return conn
}

func (k *KafkaClient) WriteMessage(key string, message interface{}) {
	k.conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	msg, marsErr := json.Marshal(message)
	if marsErr != nil {
		log.Panicf("Failed to marshal the messages: %s", marsErr.Error())
	}
	_, err := k.conn.WriteMessages(
		kafka.Message{Key: []byte(key), Value: msg})
	if err != nil {
		log.Panicf("Failed to write messages: %s", err.Error())
	}
}

func (k *KafkaClient) CloseConnection() {
	if err := k.conn.Close(); err != nil {
		log.Panicf("Failed to close writer: %s", err.Error())
	}
}

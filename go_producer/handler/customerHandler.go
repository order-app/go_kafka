package handler

import (
	"github.com/labstack/echo"
	"go_kafka/go_producer/client"
	"go_kafka/go_producer/producer"
	"net/http"
)

type CustomerHandler struct {
	kafkaClient   producer.KafkaClient
	serviceClient *client.CustomerClient
}

func NewCustomerHandler(kafkaClient producer.KafkaClient, serviceClient *client.CustomerClient) CustomerHandler {
	return CustomerHandler{
		kafkaClient:   kafkaClient,
		serviceClient: serviceClient,
	}
}

func (h *CustomerHandler) CustomersHandler(c echo.Context) error {
	customers := h.serviceClient.GetCustomers()
	for _, customer := range customers {
		h.kafkaClient.WriteMessage("customer", customer)
	}
	return c.JSON(http.StatusOK, "Customers send to kafka.")
}

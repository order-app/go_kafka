package handler

import (
	"github.com/labstack/echo"
	"go_kafka/go_producer/client"
	"go_kafka/go_producer/producer"
	"net/http"
)

type OrderHandler struct {
	kafkaClient   producer.KafkaClient
	serviceClient *client.OrderClient
}

func NewOrderHandler(kafkaClient producer.KafkaClient, serviceClient *client.OrderClient) OrderHandler {
	return OrderHandler{
		kafkaClient:   kafkaClient,
		serviceClient: serviceClient,
	}
}

func (h *OrderHandler) OrderHandler(c echo.Context) error {
	orders := h.serviceClient.GetOrders()
	for _, order := range orders {
		h.kafkaClient.WriteMessage("order", order)
	}
	return c.JSON(http.StatusOK, "Orders send to kafka")
}

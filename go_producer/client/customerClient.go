package client

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/labstack/gommon/log"
	"go_kafka/go_producer/helper/errors"
	_errorType "go_kafka/go_producer/helper/errors/type"
	_type "go_kafka/go_producer/type"
	"net/http"
	"time"
)

type CustomerClient struct {
	BaseURI    string
	HTTPClient *http.Client
}

func NewCustomerClient(baseURI string) *CustomerClient {
	return &CustomerClient{
		BaseURI:    baseURI,
		HTTPClient: &http.Client{Timeout: time.Minute},
	}
}

func (c *CustomerClient) GetCustomers() []_type.CustomerClientResponseType {
	var customersList []_type.CustomerClientResponseType
	requestURI := fmt.Sprintf("%s", c.BaseURI)
	req, err := http.NewRequestWithContext(context.Background(), "GET", requestURI, nil)
	if err != nil {
		log.Errorf("NewRequest error : %s", err.Error())
		errors.Panic(_errorType.ErrorType{
			Code:    500,
			Message: "HttpClient error.",
		})
	}
	response := c.sendRequest(req)
	totalItem := response.TotalItemCount
	totalPage := totalItem / response.PageSize
	if totalItem%response.PageSize != 0 {
		totalPage++
	}
	customersList = append(customersList, response.Data...)
	for p := 2; p <= totalPage; p++ {
		requestURI = fmt.Sprintf("%s?page=%v", c.BaseURI, p)
		req, err = http.NewRequestWithContext(context.Background(), "GET", requestURI, nil)
		if err != nil {
			log.Errorf("NewRequest error : %s", err.Error())
			errors.Panic(_errorType.ErrorType{
				Code:    500,
				Message: "HttpClient error.",
			})
		}
		response = c.sendRequest(req)
		customersList = append(customersList, response.Data...)
	}
	return customersList
}

func (c *CustomerClient) sendRequest(req *http.Request) _type.CustomersClientResponseType {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")

	res, doErr := c.HTTPClient.Do(req)
	if doErr != nil {
		log.Errorf("HttpClient Do error : %s", doErr)
		errors.Panic(_errorType.ErrorType{
			Code:    500,
			Message: "HttpClient error.",
		})
	}
	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes *_errorType.ErrorType
		if dErr := json.NewDecoder(res.Body).Decode(&errRes); dErr != nil {
			log.Errorf("Send request decode error message error : %s", dErr.Error())
			errors.Panic(_errorType.ErrorType{
				Code:    500,
				Message: "HttpClient error.",
			})
		}
		errors.Panic(_errorType.ErrorType{Code: errRes.Code, Message: errRes.Message})
	}

	var response _type.CustomersClientResponseType
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(_errorType.ErrorType{
			Code:    500,
			Message: "HttpClient error.",
		})
	}
	return response
}

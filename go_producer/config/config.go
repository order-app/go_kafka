package config

type ClientURIConfig struct {
	CustomerClient string
	OrderClient    string
}

var EnvClientURIConfig = map[string]ClientURIConfig{
	"local": {
		CustomerClient: "http://localhost:8080/customer",
		OrderClient:    "http://localhost:8090/order",
	},
}

package errors

import _type "go_kafka/go_producer/helper/errors/type"

func Panic(err _type.ErrorType) {
	panic(&err)
}

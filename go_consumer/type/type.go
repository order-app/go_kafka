package _type

type Message struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}
type Address struct {
	AddressLine string `json:"addressLine"`
	City        string `json:"city"`
	Country     string `json:"country"`
	CityCode    int    `json:"cityCode"`
}

package errors

import _type "go_kafka/go_consumer/helper/errors/errorType"

func Panic(err _type.ErrorType) {
	panic(&err)
}

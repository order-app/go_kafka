package errorType

type ErrorType struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

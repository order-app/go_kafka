package config

type Config struct {
	Host     string
	Port     string
	UserName string
	Password string
	DbName   string
}

var EnvConfig = map[string]Config{
	"local": {
		Host:     "localhost",
		Port:     "8080",
		UserName: "ordercustomer",
		Password: "12345",
		DbName:   "kafka",
	},
}

var CollectionNameConfig = map[string]string{
	"customer": "customer",
	"order":    "order",
}

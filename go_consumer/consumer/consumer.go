package consumer

import (
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"go_kafka/go_consumer/helper/errors"
	"go_kafka/go_consumer/helper/errors/errorType"
	"go_kafka/go_consumer/type"
	"log"
	"net/http"
	"time"
)

type KafkaClient struct {
	reader *kafka.Reader
}

func NewKafkaClient(brokerAddress string, topic string, partition int) KafkaClient {
	r := createReader(topic, partition, brokerAddress)
	return KafkaClient{reader: r}
}

func createReader(topic string, partition int, brokerAddress string) *kafka.Reader {
	cnfg := kafka.ReaderConfig{
		Brokers:   []string{brokerAddress},
		Topic:     topic,
		Partition: partition,
		MinBytes:  10e3,
		MaxBytes:  10e6,
		MaxWait:   10 * time.Second,
	}
	r := kafka.NewReader(cnfg)
	//r.SetOffset(7)
	return r
}

func (k *KafkaClient) CloseConnection() {
	if err := k.reader.Close(); err != nil {
		log.Panicf("Failed to close reader: %s", err.Error())
	}
}

func (k *KafkaClient) ReadMessage(msgChn chan _type.Message, errChan chan error) {
	for {
		msg, err := k.reader.ReadMessage(context.Background())
		if err != nil {
			errChan <- err
			break
		}
		message := declareMessage(msg.Key, msg.Value).(_type.Message)
		msgChn <- message
	}
}

func declareMessage(msgKey []byte, value []byte) interface{} {
	key := string(msgKey)
	switch key {
	case "customer":
		var customer _type.CustomerClientResponseType
		if errU := json.Unmarshal(value, &customer); errU != nil {
			errors.Panic(errorType.ErrorType{Code: http.StatusInternalServerError, Message: "Unmarshal error"})
		}
		message := _type.Message{
			Key:   key,
			Value: customer,
		}
		return message
	case "order":
		var order _type.OrderClientResponseType
		if errU := json.Unmarshal(value, &order); errU != nil {
			errors.Panic(errorType.ErrorType{Code: http.StatusInternalServerError, Message: "Unmarshal error"})
		}
		message := _type.Message{
			Key:   key,
			Value: order,
		}
		return message
	default:
		message := _type.Message{
			Key:   key,
			Value: string(value),
		}
		return message
	}
}

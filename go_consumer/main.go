package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go_kafka/go_consumer/config"
	"go_kafka/go_consumer/consumer"
	"go_kafka/go_consumer/storage/mongodb"
	"go_kafka/go_consumer/type"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	brokerAddress = "localhost:9092"
)

var (
	cfg         config.Config
	mongoClient *mongo.Client
	db          *mongo.Database
	customerCol *mongo.Collection
	orderCol    *mongo.Collection
)

func init() {
	cfg = config.EnvConfig["local"]
	connectionURI := fmt.Sprintf("mongodb+srv://%s:%s@cluster0.ng3fk.mongodb.net/%s?retryWrites=true&w=majority", cfg.UserName, cfg.Password, cfg.DbName)
	clientOpts := options.Client().ApplyURI(connectionURI)
	mongoClient, conErr := mongo.Connect(context.Background(), clientOpts)
	if conErr != nil {
		log.Panicf("Somethings went wrong when connecting mongodb : %s", conErr.Error())
	}
	db = mongoClient.Database(cfg.DbName)
	customerCol = db.Collection(config.CollectionNameConfig["customer"])
	orderCol = db.Collection(config.CollectionNameConfig["order"])
}

func main() {
	topic := "purchases"
	partition := 0

	kafkaClient := consumer.NewKafkaClient(brokerAddress, topic, partition)
	defer kafkaClient.CloseConnection()
	customerRepo := mongodb.NewRepository(customerCol)
	orderRepo := mongodb.NewRepository(orderCol)

	msgChan := make(chan _type.Message)
	errChan := make(chan error)
	fmt.Println("Consumer started ")
	go kafkaClient.ReadMessage(msgChan, errChan)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	msgCount := 0
	doneCh := make(chan struct{})

	go func() {
		for {
			select {
			case err := <-errChan:
				fmt.Println(err)
			case msg := <-msgChan:
				msgCount++
				fmt.Printf("Received message Count %d: | Message Key(%s) | Message Value(%s) \n", msgCount, msg.Key, msg.Value)
				if msg.Key == "customer" {
					customer := msg.Value.(_type.CustomerClientResponseType)
					customerRepo.InsertOne(customer)
				}
				if msg.Key == "order" {
					order := msg.Value.(_type.OrderClientResponseType)
					orderRepo.InsertOne(order)
				}
			case <-sigChan:
				fmt.Println("Interrupt is detected")
				doneCh <- struct{}{}
			}
		}
	}()

	<-doneCh
}

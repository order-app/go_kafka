package mongodb

import (
	"context"
	"log"
)

type Repository struct {
	collection MongoCollection
}

func NewRepository(collection MongoCollection) *Repository {
	return &Repository{collection: collection}
}

func (r *Repository) InsertOne(message interface{}) interface{} {
	res, err := r.collection.InsertOne(context.Background(), message)
	if err != nil {
		log.Printf("Unable to insert the object")
		return nil
	}
	return res.InsertedID
}
